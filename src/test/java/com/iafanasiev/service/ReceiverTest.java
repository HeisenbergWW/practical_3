package com.iafanasiev.service;

import com.iafanasiev.util.MessageProcessor;
import jakarta.jms.*;
import org.apache.activemq.ActiveMQMessageConsumer;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

import static com.iafanasiev.service.Sender.POISON_PILL_MSG;
import static org.awaitility.Awaitility.await;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;

class ReceiverTest {
    private static final Logger LOG = LoggerFactory.getLogger(ReceiverTest.class);
    @Mock
    Connection connection;
    @Mock
    private Session session;
    @Mock
    private ActiveMQMessageConsumer consumer;
    @Mock
    private TextMessage textMessage1, textMessage2, poisonPillMessage;
    @Mock
    private Destination destination;
    @Mock
    MessageProcessor messageProcessor;

    Receiver receiver;

    @BeforeEach
    void setUp() throws JMSException {
        MockitoAnnotations.openMocks(this);

        when(connection.createSession(false, Session.AUTO_ACKNOWLEDGE)).thenReturn(session);
        when(session.createConsumer(destination)).thenReturn(consumer);

        when(textMessage1.getText()).thenReturn("Message 1");
        when(textMessage2.getText()).thenReturn("Message 2");

        when(poisonPillMessage.getText()).thenReturn(POISON_PILL_MSG);
        receiver = new Receiver(connection);
    }

    @Test
    void readAndWriteMsgsTest() throws Exception {
        when(consumer.receive()).thenReturn(textMessage1, textMessage2, poisonPillMessage);

        receiver.readAndWriteMsgs(destination, messageProcessor);

        verify(messageProcessor, times(1)).processReceivedMessage(textMessage1.getText());
        verify(messageProcessor, times(1)).processReceivedMessage(textMessage2.getText());

        verify(messageProcessor, times(2)).processReceivedMessage(anyString());

        assertTrue(receiver.poisonPillReceived.get());

        verify(consumer, times(2)).close();
    }

    @Test
    void concurrentEmptyConsumerTest() throws Exception {
        reset(connection);

        Session session1 = mock(Session.class);
        Session session2 = mock(Session.class);

        consumer = mock(ActiveMQMessageConsumer.class);
        ActiveMQMessageConsumer emptyConsumer = mock(ActiveMQMessageConsumer.class);

        when(connection.createSession(false, Session.AUTO_ACKNOWLEDGE))
                .thenReturn(session1)
                .thenReturn(session2);

        when(session1.createConsumer(destination)).thenReturn(consumer);
        when(session2.createConsumer(destination)).thenReturn(emptyConsumer);

        AtomicBoolean emptyConsumerInvokedReceive = new AtomicBoolean(false);
        AtomicBoolean consumerClosedEmptyConsumer = new AtomicBoolean(false);

        when(consumer.receive())
                .thenReturn(textMessage1)
                .thenReturn(textMessage2)
                .thenAnswer(invocation -> {
                    LOG.info("Consumer0 wait for emptyConsumer invoke receive()");
                    await().atMost(1000, TimeUnit.MILLISECONDS)
                            .until(() -> {
                                LOG.info("check");
                                return emptyConsumerInvokedReceive.get();
                            });

                    return poisonPillMessage;
                });

        when(emptyConsumer.receive())
                .thenAnswer(invocation -> {
                    emptyConsumerInvokedReceive.set(true);
                    LOG.info("Block emptyConsumer by receive()");
                    await().atMost(1000, TimeUnit.MILLISECONDS)
                            .until(() -> {
                                LOG.info("check consumer closed emptyConsumer");
                                return consumerClosedEmptyConsumer.get();
                            });
                    LOG.info("Unblock emptyConsumer");
                    return null;
                });

        doAnswer(invocation -> {
            consumerClosedEmptyConsumer.set(true);
            return null;
        }).when(emptyConsumer).close();

        Thread thread1 = new Thread(() -> receiver.readAndWriteMsgs(destination, messageProcessor));
        Thread thread2 = new Thread(() -> receiver.readAndWriteMsgs(destination, messageProcessor));

        thread1.start();
        thread2.start();

        thread1.join();
        thread2.join();

        verify(messageProcessor, times(1)).processReceivedMessage("Message 1");
        verify(messageProcessor, times(1)).processReceivedMessage("Message 2");

        verify(messageProcessor, times(2)).processReceivedMessage(anyString());

        verify(consumer, atLeastOnce()).close();
        verify(emptyConsumer, atLeastOnce()).close();
    }

    @Test
    void poisonPillProcessingTest() throws Exception {
        when(consumer.receive()).thenReturn(poisonPillMessage);

        receiver.readAndWriteMsgs(destination, messageProcessor);

        assertTrue(receiver.poisonPillReceived.get(), "Poison pill flag should be true after processing poison pill message.");
    }
}