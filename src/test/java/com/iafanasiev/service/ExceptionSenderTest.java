package com.iafanasiev.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.iafanasiev.exception.SenderException;
import jakarta.jms.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static com.iafanasiev.service.Sender.POISON_PILL_MSG;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

class ExceptionSenderTest {
    @Mock
    Connection connection;
    @Mock
    private Session session;
    @Mock
    private MessageProducer producer;
    @Mock
    private Destination destination;
    @Mock
    ObjectMapper objectMapper;

    Sender sender;

    @BeforeEach
    void setUp() throws JMSException {
        MockitoAnnotations.openMocks(this);

        when(connection.createSession(false, Session.AUTO_ACKNOWLEDGE)).thenReturn(session);
        when(session.createProducer(destination)).thenReturn(producer);

        sender = new Sender(objectMapper);
    }

    @Test
    void senderJmsInitializationExceptionTest() throws JMSException {
        when(session.createProducer(destination)).thenThrow(new JMSException("Mocked JMSException"));

        SenderException exception1 = assertThrows(SenderException.class, () ->
                sender.generateAndSendMessages(connection, destination, 5));

        assertEquals("Unable to create session or producer", exception1.getMessage());
        assertEquals(Thread.currentThread().getName(), exception1.getThreadName());

        /*___________________________________________________________________________________*/

        reset(session);
        when(session.createProducer(destination)).thenReturn(producer);

        doThrow(new JMSException("Mocked JMSException")).when(producer).setDeliveryMode(DeliveryMode.NON_PERSISTENT);

        SenderException exception2 = assertThrows(SenderException.class, () ->
                sender.generateAndSendMessages(connection, destination, 5));

        assertEquals("Unable to create session or producer", exception2.getMessage());
        assertEquals(Thread.currentThread().getName(), exception2.getThreadName());

        /*___________________________________________________________________________________*/

        when(connection.createSession(false, Session.AUTO_ACKNOWLEDGE)).thenThrow(new JMSException("Mocked JMSException"));

        SenderException exception3 = assertThrows(SenderException.class, () ->
                sender.generateAndSendMessages(connection, destination, 5));

        assertEquals("Unable to create session or producer", exception3.getMessage());
        assertEquals(Thread.currentThread().getName(), exception3.getThreadName());
    }

    @Test
    void senderSendPoisonPillExceptionTest() throws JMSException {
        when(session.createTextMessage(POISON_PILL_MSG)).thenThrow(new JMSException("Mocked JMSException"));

        SenderException exception = assertThrows(SenderException.class, () ->
                sender.generateAndSendMessages(connection, destination, 5));

        assertEquals("Exception when tried create and send poison-pill", exception.getMessage());
        assertEquals(Thread.currentThread().getName(), exception.getThreadName());

        /*___________________________________________________________________________________*/

        reset(session);
        when(session.createProducer(destination)).thenReturn(producer);
        TextMessage poisonPillTextMsg = mock(TextMessage.class);
        when(session.createTextMessage(POISON_PILL_MSG)).thenReturn(poisonPillTextMsg);

        doThrow(new JMSException("Mocked JMSException")).when(producer).send(poisonPillTextMsg);

        SenderException exception2 = assertThrows(SenderException.class, () ->
                sender.generateAndSendMessages(connection, destination, 5));

        assertEquals("Exception when tried create and send poison-pill", exception2.getMessage());
        assertEquals(Thread.currentThread().getName(), exception2.getThreadName());
    }
}