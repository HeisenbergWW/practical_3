package com.iafanasiev.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.iafanasiev.dto.QueueMessageDTO;
import com.iafanasiev.generator.QueueMessageGenerator;
import jakarta.jms.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDateTime;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static com.iafanasiev.service.Sender.POISON_PILL_MSG;
import static org.awaitility.Awaitility.await;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

class SenderTest {
    private static final Logger LOG = LoggerFactory.getLogger(SenderTest.class);
    @Mock
    Connection connection;
    @Mock
    private Session session;
    @Mock
    private MessageProducer producer;
    @Mock
    private Destination destination;
    @Mock
    ObjectMapper objectMapper;

    Sender sender;

    @BeforeEach
    void setUp() throws JMSException, JsonProcessingException {
        MockitoAnnotations.openMocks(this);

        when(connection.createSession(false, Session.AUTO_ACKNOWLEDGE)).thenReturn(session);
        when(session.createProducer(destination)).thenReturn(producer);

        when(session.createTextMessage(anyString()))
                .thenAnswer(invocation -> {
                    TextMessage message = Mockito.mock(TextMessage.class);
                    when(message.getText()).thenReturn(invocation.getArgument(0));
                    return message;
                });

        when(objectMapper.writeValueAsString(QueueMessageDTO.class)).thenReturn("{\"mocked\":\"json\"}");
        sender = new Sender(objectMapper);
    }

    @Test
    void generateAndSendMessagesTest() throws JMSException {
        int numMessages = 5;

        QueueMessageDTO testQueueMessageDTO = new QueueMessageDTO("name", "eddr", 1, LocalDateTime.now());
        try (MockedStatic<QueueMessageGenerator> utilities = Mockito.mockStatic(QueueMessageGenerator.class)) {
            utilities.when(QueueMessageGenerator::generateQueueMessage)
                    .thenReturn(testQueueMessageDTO);

            sender.generateAndSendMessages(connection, destination, numMessages);
            utilities.verify(QueueMessageGenerator::generateQueueMessage, atLeast(numMessages));
        }

        ArgumentCaptor<TextMessage> textMessageCaptor = ArgumentCaptor.forClass(TextMessage.class);

        verify(producer, times(numMessages + 1)).send(textMessageCaptor.capture());

        List<TextMessage> allMessages = textMessageCaptor.getAllValues();
        String lastMsg = allMessages.getLast().getText();

        assertEquals(POISON_PILL_MSG, lastMsg, "The poison pill message should be the last message sent.");
        LOG.info("Verify poison pill passed");
    }

    @Test
    void sendPoisonPillTest() throws JMSException {
        sender.sendPoisonPill(session, producer);

        ArgumentCaptor<TextMessage> messageCaptor = ArgumentCaptor.forClass(TextMessage.class);

        verify(producer).send(messageCaptor.capture());
        TextMessage capturedMessage = messageCaptor.getValue();

        assertEquals(POISON_PILL_MSG, capturedMessage.getText(), "The sent message should be the poison pill.");
    }

    @Test
    void generationTimeoutInSecondsTest() {
        long generationTime = 2;
        sender.startPoisonPillTimer(generationTime);

        await().atMost(2100, TimeUnit.MILLISECONDS).untilTrue(sender.needStop);

        assertTrue(sender.needStop.get(), "The needStop flag should be set to true after the timer runs.");
    }
}