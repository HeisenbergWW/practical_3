package com.iafanasiev.service;

import com.iafanasiev.exception.ReceiverException;
import com.iafanasiev.util.MessageProcessor;
import jakarta.jms.*;
import org.apache.activemq.ActiveMQMessageConsumer;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static com.iafanasiev.service.Sender.POISON_PILL_MSG;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class ReceiverExceptionTest {
    @Mock
    Destination destination;
    @Mock
    Connection connection;
    @Mock
    Session session;
    @Mock
    ActiveMQMessageConsumer consumer;
    @Mock
    MessageProcessor messageProcessor;

    Receiver receiver;

    @BeforeEach
    void setUp() throws JMSException {
        MockitoAnnotations.openMocks(this);
        when(connection.createSession(false, Session.AUTO_ACKNOWLEDGE)).thenReturn(session);
        when(session.createConsumer(destination)).thenReturn(consumer);

        receiver = new Receiver(connection);
    }

    @Test
    void receiveReceiverExceptionTest() throws JMSException {
        when(consumer.receive()).thenThrow(new JMSException("JMS error"));

        ReceiverException exception = assertThrows(ReceiverException.class, () ->
                receiver.readAndWriteMsgs(destination, messageProcessor));

        assertTrue(exception.getMessage().contains("Error when consumer invoked the receive method"), "Actual: " + exception.getMessage());
        assertEquals(Thread.currentThread().getName(), exception.getThreadName());
    }

    @Test
    void receiveNoWaitReceiverExceptionTest() throws JMSException {
        when(consumer.receiveNoWait()).thenThrow(new JMSException("JMS error"));
        receiver.poisonPillReceived.set(true);
        when(consumer.getMessageSize()).thenReturn(1);

        ReceiverException exception = assertThrows(ReceiverException.class, () ->
                receiver.readAndWriteMsgs(destination, messageProcessor));

        assertEquals("Error when consumer invoked the receiveNoWait method", exception.getMessage());
        assertEquals(Thread.currentThread().getName(), exception.getThreadName());
    }

    @Test
    void closeConsumerReceiverExceptionTest() throws JMSException {
        TextMessage textMessage = mock(TextMessage.class);
        when(textMessage.getText()).thenReturn(POISON_PILL_MSG);
        when(consumer.receive()).thenReturn(textMessage);

        doThrow(new JMSException("JMS error")).when(consumer).close();

        ReceiverException exception = assertThrows(ReceiverException.class, () ->
                receiver.readAndWriteMsgs(destination, messageProcessor));

        assertEquals("Error when closing consumer with no unconsumed messages: " + consumer, exception.getMessage());
        assertEquals(Thread.currentThread().getName(), exception.getThreadName());
    }

    @Test
    void initializationSessionOrConsumerReceiverExceptionTest() throws JMSException {
        reset(session);
        when(session.createConsumer(destination)).thenThrow(new JMSException("JMS error"));

        ReceiverException exception = assertThrows(ReceiverException.class, () ->
                receiver.readAndWriteMsgs(destination, messageProcessor));

        assertEquals("Error while creating session or consumer. Destination: " + destination, exception.getMessage());
        assertEquals(Thread.currentThread().getName(), exception.getThreadName());

        /*________________________________________________*/

        reset(connection);
        when(connection.createSession(false, Session.AUTO_ACKNOWLEDGE)).thenThrow(new JMSException("JMS error"));

        ReceiverException exception2 = assertThrows(ReceiverException.class, () ->
                receiver.readAndWriteMsgs(destination, messageProcessor));

        assertEquals("Error while creating session or consumer. Destination: " + destination, exception2.getMessage());
        assertEquals(Thread.currentThread().getName(), exception2.getThreadName());
    }

    @Test
    void getTextReceiverExceptionTest() throws JMSException {
        TextMessage textMessage = mock(TextMessage.class);
        when(textMessage.getText()).thenThrow(new JMSException("JMS error"));
        when(consumer.receive()).thenReturn(textMessage);

        ReceiverException exception = assertThrows(ReceiverException.class, () ->
                receiver.readAndWriteMsgs(destination, messageProcessor));

        assertEquals("Error while getting text from the message", exception.getMessage());
        assertEquals(Thread.currentThread().getName(), exception.getThreadName());
    }
}