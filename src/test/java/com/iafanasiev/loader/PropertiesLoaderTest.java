package com.iafanasiev.loader;

import org.junit.jupiter.api.Test;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import static org.junit.jupiter.api.Assertions.*;

class PropertiesLoaderTest {

    @Test
    void validFileTest() throws IOException {
        String fileName = "test.properties";

        Properties properties = PropertiesLoader.loadProperties(fileName);

        assertNotNull(properties);
        assertEquals("tcp://localhost:61616", properties.getProperty("broker.url"));
        assertEquals("MyQueue", properties.getProperty("queue.name"));
        assertEquals("1000", properties.getProperty("messages.number"));
        assertEquals("30", properties.getProperty("generation.timeout.seconds"));
        assertEquals("Afanasiev", properties.getProperty("username"));
        assertEquals("AfanasievPassword", properties.getProperty("password"));
    }

    @Test
    void fileNotFoundTest() {
        String fileName = "nonexistent.properties";

        FileNotFoundException exception = assertThrows(FileNotFoundException.class, () -> PropertiesLoader.loadProperties(fileName));
        assertEquals("Could not find properties file: " + fileName, exception.getMessage());
    }
    @Test
    void createPrivateConstructor() {
        assertThrows(IllegalStateException.class, PropertiesLoader::new);
    }
}
