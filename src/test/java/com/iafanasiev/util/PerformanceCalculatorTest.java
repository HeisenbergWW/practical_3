package com.iafanasiev.util;

import org.junit.jupiter.api.Test;

import static com.iafanasiev.util.PerformanceCalculator.calculateUnitsPerSecond;
import static org.junit.jupiter.api.Assertions.assertEquals;


class PerformanceCalculatorTest {

    @Test
    void calculateUnitsPerSecondTest() {
       long timeMs = 38618;

       assertEquals(25894.66,calculateUnitsPerSecond(timeMs,1000000));
    }
}