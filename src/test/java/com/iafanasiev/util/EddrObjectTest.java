package com.iafanasiev.util;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.*;

class EddrObjectTest {
    @ParameterizedTest
    @CsvSource({
            "19710303-80592, 19710303, 8059, 2",
            "19180329-90679, 19180329, 9067, 9",
            "1969031368060, 19690313, 6806, 0"
    })
    void parseEddrValuesTest(String eddr, String datePart, String recordNum, int controlNum) {
        EddrObject eddrObject = new EddrObject(eddr);
        assertEquals(datePart, eddrObject.getDatePart());
        assertEquals(recordNum, eddrObject.getRecordNum());
        assertEquals(controlNum, eddrObject.getControlNum());
    }

    @ParameterizedTest
    @CsvSource({
            "197103038059, 2",
            "191803299067, 9",
            "196903136806, 0"
    })
    void calculateControlNumTest(String eddrWithoutControlDigit, int expectedNum) {
        int result = EddrObject.calculateControlNum(eddrWithoutControlDigit);
        assertEquals(expectedNum, result);
    }
}