package com.iafanasiev.validator.hasletter;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.mockito.Mock;

import jakarta.validation.ConstraintValidatorContext;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class HasLetterValidatorTest {
    private HasLetterValidator validator;
    @Mock
    ConstraintValidatorContext context;
    @Mock
    ConstraintValidatorContext.ConstraintViolationBuilder constraintViolationBuilder;
    @BeforeEach
    public void setUp() {
        validator = new HasLetterValidator();
        context = mock(ConstraintValidatorContext.class);

        HasLetter constraintAnnotation = mock(HasLetter.class);
        when(constraintAnnotation.value()).thenReturn('a');
        validator.initialize(constraintAnnotation);
    }

    @Test
    void initializeTest() {
        assertEquals('a', validator.letter);
    }
    @ParameterizedTest
    @CsvSource({
            "abc, true",
            "xyz, false",
            "Aaaa, true",
            ", true",
            "a, true",
            "bbb, false",
            "ba, true"
    })
    void isValid(String value, boolean expected) {
        boolean result = validator.isValid(value, context);
        assertEquals(expected, result);
    }
}