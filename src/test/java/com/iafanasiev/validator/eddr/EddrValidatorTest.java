package com.iafanasiev.validator.eddr;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import jakarta.validation.ConstraintValidatorContext;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

class EddrValidatorTest {
    private EddrValidator validator;

    @Mock
    private ConstraintValidatorContext context;

    @Mock
    private ConstraintValidatorContext.ConstraintViolationBuilder constraintViolationBuilder;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        validator = new EddrValidator();
        when(context.buildConstraintViolationWithTemplate(anyString())).thenReturn(constraintViolationBuilder);
    }

    @Test
    void testIsValid_NullEddr() {
        assertTrue(validator.isValid(null, context));
    }

    @ParameterizedTest
    @CsvSource({
            "2025030-380592",
            "20250303805920",
            "\"\"",
            "''",
            "2",
            "18350303-805920"
    })
    void testIsValid_InvalidPattern(String eddr) {
        assertFalse(validator.isValid(eddr, context));
        verify(context).disableDefaultConstraintViolation();
        verify(context).buildConstraintViolationWithTemplate("Eddr must match the pattern 'YYYYMMDD-NNNNN' or 'YYYYMMDDNNNNN'");
    }

    @ParameterizedTest
    @CsvSource({
            "20250303-80592",
            "2025030380592",
            "18350303-80592"
    })
    void testIsValid_InvalidYear(String eddr) {
        assertFalse(validator.isValid(eddr, context));
        verify(context).disableDefaultConstraintViolation();
        verify(context).buildConstraintViolationWithTemplate("Invalid year in EDDR");
    }

    @ParameterizedTest
    @CsvSource({
            "20240003-80592",
            "2024240380592",
            "20241303-80592"
    })
    void testIsValid_InvalidMonth(String eddr) {
        assertFalse(validator.isValid(eddr, context));
        verify(context).disableDefaultConstraintViolation();
        verify(context).buildConstraintViolationWithTemplate("Invalid month in EDDR");
    }

    @ParameterizedTest
    @CsvSource({
            "20240100-80592",
            "20240132-80592",
            "20240230-80592",
            "20230229-80592",
            "2024033280592",
            "20240431-80592",
            "2024053280592",
            "20240631-80592",
            "2024073280592",
    })
    void testIsValid_InvalidDay(String eddr) {
        assertFalse(validator.isValid(eddr, context));
        verify(context).disableDefaultConstraintViolation();
        verify(context).buildConstraintViolationWithTemplate("Invalid day in EDDR");
    }

    @ParameterizedTest
    @CsvSource({
            "20240111-80592",
            "20240111-80592",
            "20240211-80592",
            "20230211-80592",
            "2024031180592",
            "20240411-80592",
            "2024051132592",
            "2024071180326",
            "20240612-85362",
            "2024071180592"
    })
    void testIsValid_InvalidControlNum(String eddr) {
        assertFalse(validator.isValid(eddr, context));
        verify(context).disableDefaultConstraintViolation();
        verify(context).buildConstraintViolationWithTemplate("Invalid control number in EDDR");
    }

    @ParameterizedTest
    @CsvSource({
            "19710303-80592",
            "1971030380592",
            "19180329-90679",
            "19690313-68060",
            "20220521-44783",
            "19841012-17383",
            "19630306-25629",
            "19220209-92655",
            "19740103-81048",
            "19210627-68349",
            "19021020-70622",
            "20210415-70235",
            "19900208-29299",
            "19340429-15132",
            "19160126-62240",
            "19180821-75189",
            "20180206-92107",
            "19460406-26947",
            "19070826-46249",
            "20040923-17875",
            "19710529-51589",
            "19010923-75717",
            "19690712-78420",
            "19720714-52918"
    })
    void testIsValid_ValidEddr(String eddr) {
        assertTrue(validator.isValid(eddr, context));
    }
}