package com.iafanasiev.generator;

import com.iafanasiev.dto.QueueMessageDTO;
import com.iafanasiev.util.EddrObject;
import org.apache.commons.lang3.RandomStringUtils;

import java.time.LocalDateTime;
import java.util.Random;


public class QueueMessageGenerator {
    private QueueMessageGenerator() {
    }

    static Random random = new Random();

    public static QueueMessageDTO generateQueueMessage() {
        int randomNameBound = 100;

        String name = RandomStringUtils.secure().nextAlphabetic(random.nextInt(randomNameBound));
        if (random.nextInt(100) < 70) {
            name += "a";
        }
        String eddr = generateEddr();

        int randomCountBound = 500;

        int count = random.nextInt(randomCountBound);
        LocalDateTime createdAt = LocalDateTime.now();

        return new QueueMessageDTO(name, eddr, count, createdAt);
    }

    private static String generateEddr() {
        String datePart = generateRandomDatePart();
        String recordNum = String.format("%04d", random.nextInt(9999));
        String controlNum = String.valueOf(EddrObject.calculateControlNum(datePart + recordNum));

        return datePart + recordNum + controlNum;
    }

    private static String generateRandomDatePart() {
        int year = 1900 + random.nextInt(150);
        int month = 1 + random.nextInt(13);
        int day = 1 + random.nextInt(33);

        return String.format("%04d%02d%02d", year, month, day);
    }
}
