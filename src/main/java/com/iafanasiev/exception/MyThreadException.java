package com.iafanasiev.exception;

public abstract class MyThreadException extends RuntimeException {
    private final String threadName;

    protected MyThreadException(String message, String threadName, Throwable cause) {
        super(message, cause);
        this.threadName = threadName;
    }

    public String getThreadName() {
        return threadName;
    }
}