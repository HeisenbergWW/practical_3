package com.iafanasiev.exception;

public class MessageProcessingException extends MyThreadException {
    public MessageProcessingException(String message, String threadName, Throwable cause) {
        super(message, threadName, cause);
    }
}