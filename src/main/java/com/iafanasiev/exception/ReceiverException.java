package com.iafanasiev.exception;

public class ReceiverException extends MyThreadException {
    public ReceiverException(String message, String threadName, Throwable cause) {
        super(message, threadName, cause);
    }
}