package com.iafanasiev.exception;

public class SenderException extends MyThreadException {
    public SenderException(String message, String threadName, Throwable cause) {
        super(message, threadName, cause);
    }
}