package com.iafanasiev.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.IntNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.node.TextNode;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.iafanasiev.dto.QueueMessageDTO;
import com.iafanasiev.exception.MessageProcessingException;
import jakarta.validation.ConstraintViolation;
import jakarta.validation.Validator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class MessageProcessor {
    static final Logger LOGGER = LoggerFactory.getLogger(MessageProcessor.class);
    private static final String OUTPUT_DIR_NAME = "output";
    public static final String VALID_MESSAGES_FILE = String.valueOf(Path.of(OUTPUT_DIR_NAME, "valid_messages.csv"));
    public static final String INVALID_MESSAGES_FILE = String.valueOf(Path.of(OUTPUT_DIR_NAME, "invalid_messages.csv"));

    private static final String ERROR_FIELD_NAME = "errors";

    static CsvSchema validCsvSchema = buildValidCsvSchema();
    static CsvSchema invalidCsvSchema = buildInvalidCsvSchema(validCsvSchema);
    static ObjectMapper objectMapper = new ObjectMapper();
    static CsvMapper csvMapper = new CsvMapper();

    static {
        objectMapper.registerModule(new JavaTimeModule());
    }

    Validator validator;
    OutputStreamWriter validWriter;
    OutputStreamWriter invalidWriter;

    public MessageProcessor(Validator validator) {
        this.validator = validator;
        try {
            this.validWriter = new OutputStreamWriter(new FileOutputStream(VALID_MESSAGES_FILE, true),
                    StandardCharsets.UTF_8);
            this.invalidWriter = new OutputStreamWriter(new FileOutputStream(INVALID_MESSAGES_FILE, true),
                    StandardCharsets.UTF_8);
        } catch (IOException e) {
            throw new MessageProcessingException("Failed to initialize output files for MessageProcessor",
                    Thread.currentThread().getName(), e);
        }
    }

    public void processReceivedMessage(String queueMessageText) {
        try {
            QueueMessageDTO messagePOJO = objectMapper.readValue(queueMessageText, QueueMessageDTO.class);

            ObjectNode node = buildNodeForMessage(messagePOJO, objectMapper);

            Set<ConstraintViolation<QueueMessageDTO>> constraintViolations =
                    validator.validate(messagePOJO);

            if (!constraintViolations.isEmpty()) {
                List<String> errors = constraintViolations.stream()
                        .map(ConstraintViolation::getMessage)
                        .toList();

                node.put(ERROR_FIELD_NAME, convertErrorsToJson(errors, objectMapper));

                saveMessage(invalidWriter, invalidCsvSchema, node, csvMapper);
            } else {
                saveMessage(validWriter, validCsvSchema, node, csvMapper);
            }

        } catch (JsonProcessingException e) {
            throw new MessageProcessingException("Error processing JSON message", Thread.currentThread().getName(), e);
        }
    }

    private void saveMessage(OutputStreamWriter writer, CsvSchema csvSchema, Object message, CsvMapper csvMapper) {
        try {
            csvMapper.writerFor(message.getClass())
                    .with(csvSchema)
                    .writeValues(writer)
                    .write(message);
        } catch (IOException e) {
            throw new MessageProcessingException("Error writing message to CSV file: " + message,
                    Thread.currentThread().getName(), e);
        }
    }

    private ObjectNode buildNodeForMessage(QueueMessageDTO message, ObjectMapper objectMapper) {
        ObjectNode node = objectMapper.createObjectNode();
        node.set("name", TextNode.valueOf(message.getName()));
        node.set("count", IntNode.valueOf(message.getCount()));
        return node;
    }

    private String convertErrorsToJson(List<String> errors, ObjectMapper objectMapper) {
        try {
            ObjectNode rootNode = objectMapper.createObjectNode();

            ArrayNode errorsArray = objectMapper.createArrayNode();
            for (String error : errors) {
                errorsArray.add(error);
            }

            rootNode.set(ERROR_FIELD_NAME, errorsArray);

            return objectMapper.writeValueAsString(rootNode);
        } catch (JsonProcessingException e) {
            throw new MessageProcessingException("Error while converting \"errors\" to JSON. \"errors\": " + errors,
                    Thread.currentThread().getName(), e);
        }
    }

    private static void writeHeader(String fileName, CsvSchema csvSchema) {
        try (FileWriter writer = new FileWriter(fileName, false)) {
            String str = csvSchema.withHeader().getColumnDesc();
            String formattedHeaders = str.chars()
                    .filter(c -> Character.isLetter(c) || c == ',')
                    .mapToObj(c -> String.valueOf((char) c))
                    .collect(Collectors.joining(""));

            writer.write(formattedHeaders);
            writer.write("\n");
        } catch (IOException e) {
            throw new MessageProcessingException("Failed to create or write headers to file: " + fileName,
                    Thread.currentThread().getName(), e);
        }
    }

    public static void createFilesWithHeaders() {
        LOGGER.info("Creating files with headers");
        try {
            Files.createDirectories(Path.of(OUTPUT_DIR_NAME));
            writeHeader(VALID_MESSAGES_FILE, validCsvSchema.withHeader());
            writeHeader(INVALID_MESSAGES_FILE, invalidCsvSchema.withHeader());
        } catch (IOException e) {
            throw new MessageProcessingException("Failed to create output directory: " + OUTPUT_DIR_NAME,
                    Thread.currentThread().getName(), e);
        }
    }

    private static CsvSchema buildInvalidCsvSchema(CsvSchema validCsvSchema) {
        return validCsvSchema.rebuild()
                .addColumn(ERROR_FIELD_NAME)
                .build();
    }

    private static CsvSchema buildValidCsvSchema() {
        return CsvSchema.builder()
                .addColumn("name")
                .addColumn("count")
                .build();
    }
}