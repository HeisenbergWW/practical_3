package com.iafanasiev.util;

public class PerformanceCalculator {
    public static double calculateUnitsPerSecond(long processingMSTime, long processedQuantity) {
        double result = (double) (processedQuantity * 1000) / processingMSTime;
        double count = 100.0;
        return Math.round(result * count) / count;
    }

    private PerformanceCalculator() {
    }
}