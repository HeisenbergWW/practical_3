package com.iafanasiev.util;

import java.util.stream.IntStream;

public class EddrObject {
    public String getRecordNum() {
        return recordNum;
    }

    public int getControlNum() {
        return controlNum;
    }

    public String getDatePart() {
        return datePart;
    }

    String eddr;
    String datePart;
    String recordNum;
    int controlNum;


    public EddrObject(String eddr) {
        parseEddrValues(eddr);
    }

    private void parseEddrValues(String eddr) {
        String numberPart;
        if (eddr.contains("-")) {
            String[] split = eddr.split("-");
            datePart = split[0];
            numberPart = split[1];

        } else {
            datePart = eddr.substring(0, 8);
            numberPart = eddr.substring(8);
        }
        this.eddr = datePart + numberPart;

        recordNum = numberPart.substring(0, 4);
        controlNum = Integer.parseInt(numberPart.substring(4));
    }

    public static int calculateControlNum(String eddrWithoutControlDigit) {
        int[] weightFunction = {7, 3, 1};

        int sum = IntStream.range(0, eddrWithoutControlDigit.length())
                .map(i -> Character.getNumericValue(eddrWithoutControlDigit.charAt(i)) * weightFunction[i % weightFunction.length])
                .sum();

        return sum % 10;
    }
}