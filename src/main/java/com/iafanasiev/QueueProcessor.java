package com.iafanasiev;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.iafanasiev.dto.ProducerConfigDTO;
import com.iafanasiev.exception.MyThreadException;
import com.iafanasiev.loader.PropertiesLoader;
import com.iafanasiev.service.Receiver;
import com.iafanasiev.service.Sender;
import com.iafanasiev.util.MessageProcessor;
import jakarta.jms.Connection;
import jakarta.jms.Destination;
import jakarta.jms.JMSException;
import jakarta.jms.Session;
import jakarta.validation.Validation;
import jakarta.validation.ValidatorFactory;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.commons.lang3.time.StopWatch;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.concurrent.*;

import static com.iafanasiev.util.PerformanceCalculator.calculateUnitsPerSecond;

public class QueueProcessor {
    private static final Logger LOGGER = LoggerFactory.getLogger(QueueProcessor.class);

    static final String PROPERTIES_FILE_NAME = "app.properties";
    static final String MSGS_NUM_KEY = "messages.num";
    static final int DEFAULT_MSG_NUM = 1_000_000;

    static long sendAndReceiveTimeMs = 0;

    public static void main(String[] args) {

        LOGGER.info("Start of program");
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        try {
            Properties properties = PropertiesLoader.loadProperties(PROPERTIES_FILE_NAME);
            String brokerUrl = properties.getProperty("broker.url");
            String userName = properties.getProperty("broker.username");
            String password = properties.getProperty("broker.password");

            int messagesNumber = args.length > 0 ? Integer.parseInt(args[0]) : DEFAULT_MSG_NUM;

            properties.setProperty(MSGS_NUM_KEY, String.valueOf(messagesNumber));
            String queueName = properties.getProperty("queue.name");

            try (Connection connection = createActiveMQConnection(brokerUrl, userName, password)) {
                connection.start();
                Destination destination = createQueue(queueName, connection);

                processProducers(properties, connection, destination);

                processConsumers(properties, connection, destination);
                LOGGER.info("General for {} number of messages MPS: {}", messagesNumber,
                        calculateUnitsPerSecond(sendAndReceiveTimeMs, messagesNumber + 1L));
            }
        } catch (MyThreadException e) {
            LOGGER.error("Exception occurred in thread '{}': {}", e.getThreadName(), e.getMessage());
        } catch (InterruptedException e) {
            LOGGER.error(e.getMessage(), e);
            Thread.currentThread().interrupt();
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
        stopWatch.stop();
        LOGGER.info("General time of program: {} sec", stopWatch.getTime(TimeUnit.MILLISECONDS) / 1000.0);
    }

    private static void processProducers(Properties properties, Connection connection, Destination destination)
            throws InterruptedException, ExecutionException {
        ProducerConfigDTO config = parseProducerConfig(properties);

        List<Future<?>> sendFutures = new ArrayList<>();
        ExecutorService sendExecutorService = Executors.newFixedThreadPool(config.producersNum());

        StopWatch stopWatch = new StopWatch();
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.registerModule(new JavaTimeModule());

        Sender sender = new Sender(objectMapper);
        LOGGER.info("Generating and sending {} messages with {} producers", config.numMessages(), config.producersNum());
        Timer timer = sender.startPoisonPillTimer(config.generationTimeoutInSeconds());
        stopWatch.start();

        for (int i = 0; i < config.producersNum(); i++) {
            sendFutures.add(sendExecutorService.submit(() ->
                    sender.generateAndSendMessages(connection, destination, config.numMessages())));
        }

        LOGGER.info("Waiting for send threads to finish. Generation timeout: {} sec", config.generationTimeoutInSeconds());
        shutDownExecutorService(sendFutures, sendExecutorService);
        timer.cancel();

        stopWatch.stop();
        long sendMsTime = stopWatch.getTime(TimeUnit.MILLISECONDS);
        sendAndReceiveTimeMs += sendMsTime;
        double mps = calculateUnitsPerSecond(sendMsTime, config.numMessages() + 1L);
        LOGGER.info("Send executor service shut down. Sending time: {} ms, MPS: {}", sendMsTime, mps);
    }

    private static void processConsumers(Properties properties, Connection connection, Destination destination)
            throws InterruptedException, ExecutionException {
        int numMessages = Integer.parseInt(properties.getProperty(MSGS_NUM_KEY));
        int consumersNum = Integer.parseInt(properties.getProperty("consumers.number"));
        List<Future<?>> readFutures = new ArrayList<>();
        ExecutorService readExecutorService = Executors.newFixedThreadPool(consumersNum);

        ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory();
        StopWatch stopWatch = new StopWatch();
        Receiver receiver = new Receiver(connection);

        MessageProcessor.createFilesWithHeaders();
        LOGGER.info("Start reading and process {} messages. Consumers number: {}", numMessages, consumersNum);
        stopWatch.start();
        for (int i = 0; i < consumersNum; i++) {
            readFutures.add(readExecutorService.submit(() ->
                    receiver.readAndWriteMsgs(destination, new MessageProcessor(validatorFactory.getValidator()))));
        }
        LOGGER.info("Waiting for read threads to finish.");
        shutDownExecutorService(readFutures, readExecutorService);

        stopWatch.stop();
        validatorFactory.close();

        long readMsTime = stopWatch.getTime(TimeUnit.MILLISECONDS);
        sendAndReceiveTimeMs += readMsTime;
        double mps = calculateUnitsPerSecond(readMsTime, numMessages + 1L);
        LOGGER.info("Read executor service shut down. Reading time: {} ms, MPS: {}", readMsTime, mps);
    }

    private static void shutDownExecutorService(List<Future<?>> futures, ExecutorService executorService)
            throws InterruptedException, ExecutionException {
        executorService.shutdown();

        int terminationTimeout = 5;
        if (!executorService.awaitTermination(terminationTimeout, TimeUnit.MINUTES)) {
            LOGGER.warn("Executor shutdown forced after timeout of {} min.", terminationTimeout);
            executorService.shutdownNow();
        }
        for (Future<?> future : futures) {
            future.get();
        }
    }

    private static Connection createActiveMQConnection(String brokerUrl, String userName, String password)
            throws JMSException {
        LOGGER.debug("Creating ActiveMQConnection");
        ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory(brokerUrl);

        connectionFactory.setUserName(userName);
        connectionFactory.setPassword(password);

        connectionFactory.setTrustedPackages(Arrays.asList("com.iafanasiev", "org.apache.activemq"));

        return connectionFactory.createConnection();
    }

    private static Destination createQueue(String queueName, Connection connection) throws JMSException {
        try (Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE)) {
            return session.createQueue(queueName);
        }
    }

    private static ProducerConfigDTO parseProducerConfig(Properties properties) {
        int numMessages = Integer.parseInt(properties.getProperty(MSGS_NUM_KEY));
        long generationTimeout = Long.parseLong(properties.getProperty("generation.timeout.seconds"));
        int producersNum = Integer.parseInt(properties.getProperty("producers.number"));
        return new ProducerConfigDTO(numMessages, generationTimeout, producersNum);
    }
}