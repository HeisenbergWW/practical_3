package com.iafanasiev.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.iafanasiev.dto.QueueMessageDTO;
import com.iafanasiev.exception.SenderException;
import com.iafanasiev.generator.QueueMessageGenerator;
import jakarta.jms.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Stream;

public class Sender {
    private static final Logger LOGGER = LoggerFactory.getLogger(Sender.class);
    static final String POISON_PILL_MSG = "POISON_PILL";

    AtomicBoolean needStop = new AtomicBoolean(false);
    AtomicInteger count = new AtomicInteger();
    AtomicInteger producersCount = new AtomicInteger();

    ObjectMapper objectMapper;

    public Sender(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    public void generateAndSendMessages(Connection connection, Destination destination, int numMessages) {
        LOGGER.debug("Start generating and sending {} messages", numMessages);
        producersCount.incrementAndGet();

        try (Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
             MessageProducer producer = session.createProducer(destination)) {

            producer.setDeliveryMode(DeliveryMode.NON_PERSISTENT);

            Stream.generate(QueueMessageGenerator::generateQueueMessage)
                    .takeWhile(message -> !needStop.get())
                    .forEach(message -> {
                        if (count.incrementAndGet() > numMessages) {
                            needStop.set(true);
                            LOGGER.debug("Stop generating triggered by reaching number of messages");
                            return;
                        }
                        sendJsonMessage(message, session, producer, objectMapper);
                        LOGGER.trace("Was sent {}", message);
                    });

            LOGGER.debug("Producers count {}", producersCount.get());
            if (producersCount.decrementAndGet() == 0) {
                LOGGER.info("Sending poison pill");
                sendPoisonPill(session, producer);
            }
        } catch (JMSException e) {
            throw new SenderException("Unable to create session or producer", Thread.currentThread().getName(), e);
        }

        LOGGER.debug("Stop generating and sending messages");
    }

    void sendJsonMessage(QueueMessageDTO message, Session session, MessageProducer producer, ObjectMapper objectMapper) {
        try {
            String jsonMessage = objectMapper.writeValueAsString(message);

            TextMessage textMessage = session.createTextMessage(jsonMessage);

            producer.send(textMessage);
        } catch (JsonProcessingException e) {
            LOGGER.error("Error converting message to JSON: {}", message, e);
        } catch (JMSException e) {
            LOGGER.error("Error creating or sending message: {}", message, e);
        }
    }

    void sendPoisonPill(Session session, MessageProducer producer) {
        try {
            TextMessage poisonPillMessage = session.createTextMessage(POISON_PILL_MSG);
            poisonPillMessage.setJMSType(POISON_PILL_MSG);
            producer.send(poisonPillMessage);
            LOGGER.info("Poison pill was sent.");
        } catch (JMSException e) {
            throw new SenderException("Exception when tried create and send poison-pill",
                    Thread.currentThread().getName(), e);
        }
    }

    public Timer startPoisonPillTimer(long generationTime) {
        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                LOGGER.info("Poison pill timer triggered after {} seconds", generationTime);
                needStop.set(true);
            }
        }, generationTime * 1000);
        return timer;
    }
}