package com.iafanasiev.service;

import com.iafanasiev.exception.ReceiverException;
import com.iafanasiev.util.MessageProcessor;
import jakarta.jms.IllegalStateException;
import jakarta.jms.*;
import org.apache.activemq.ActiveMQMessageConsumer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Stream;

import static com.iafanasiev.service.Sender.POISON_PILL_MSG;

public class Receiver {
    private static final Logger LOGGER = LoggerFactory.getLogger(Receiver.class);

    private final List<ActiveMQMessageConsumer> allConsumers = new ArrayList<>();
    final AtomicBoolean poisonPillReceived = new AtomicBoolean(false);
    private final Connection connection;

    public Receiver(Connection connection) {
        this.connection = connection;
    }

    public void readAndWriteMsgs(Destination destination, MessageProcessor messageProcessor) {
        LOGGER.debug("Start reading messages");
        try (Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
             ActiveMQMessageConsumer consumer = (ActiveMQMessageConsumer) session.createConsumer(destination)) {

            synchronized (allConsumers) {
                allConsumers.add(consumer);
            }
            LOGGER.debug("All consumers size {}", allConsumers.size());

            Stream.generate(() -> getReceivedMessageText(consumer))
                    .takeWhile(Objects::nonNull)
                    .forEach(messageProcessor::processReceivedMessage);
        } catch (JMSException e) {
            throw new ReceiverException("Error while creating session or consumer. Destination: " + destination,
                    Thread.currentThread().getName(), e);
        }
        LOGGER.debug("Stop reading messages");
    }

    private String getReceivedMessageText(ActiveMQMessageConsumer consumer) {
        TextMessage msg = null;
        if (!poisonPillReceived.get()) {
            LOGGER.trace("Waiting to receive a message...");
            msg = safeReceive(consumer);
            LOGGER.trace("Message received: {}", msg);
        } else if (consumer.getMessageSize() > 0) {
            msg = safeReceiveNoWait(consumer);
            LOGGER.debug("Unconsumed message received; Current unconsumed messages: {}; consumer: {}",
                    consumer.getMessageSize(), consumer);
        }
        if (msg == null) {
            LOGGER.debug("Message is null.");
            return null;
        }
        if (isPoisonPill(msg)) {
            poisonPillReceived.set(true);
            LOGGER.info("Poison pill message found.");
            killEmptyConsumers(allConsumers);
            return null;
        }

        return safeGetText(msg);
    }

    private void killEmptyConsumers(List<ActiveMQMessageConsumer> allConsumers) {
        LOGGER.info("Killing consumers with no unconsumed messages.");

        for (ActiveMQMessageConsumer consumer : allConsumers) {
            if (consumer != null && consumer.getMessageSize() == 0) {
                LOGGER.debug("Kill consumer {}; unconsumed messages: {}.", consumer, consumer.getMessageSize());
                try {
                    consumer.close();
                } catch (JMSException e) {
                    throw new ReceiverException("Error when closing consumer with no unconsumed messages: " + consumer,
                            Thread.currentThread().getName(), e);
                }
            }
        }
    }

    private static TextMessage safeReceive(ActiveMQMessageConsumer consumer) {
        try {
            return (TextMessage) consumer.receive();
        } catch (JMSException e) {
            throw new ReceiverException("Error when consumer invoked the receive method", Thread.currentThread().getName(), e);
        }
    }

    private TextMessage safeReceiveNoWait(ActiveMQMessageConsumer consumer) {
        try {
            return (TextMessage) consumer.receiveNoWait();
        } catch (IllegalStateException e) {
            LOGGER.warn("Consumer: {} is already closed while attempting to process unconsumed messages", consumer, e);
            return null;
        } catch (JMSException e) {
            throw new ReceiverException("Error when consumer invoked the receiveNoWait method", Thread.currentThread().getName(), e);
        }
    }

    private static String safeGetText(TextMessage msg) {
        try {
            return msg.getText();
        } catch (JMSException e) {
            throw new ReceiverException("Error while getting text from the message", Thread.currentThread().getName(), e);
        }
    }

    private static boolean isPoisonPill(TextMessage msg) {
        return POISON_PILL_MSG.equals(safeGetText(msg));
    }
}