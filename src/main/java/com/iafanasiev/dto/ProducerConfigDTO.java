package com.iafanasiev.dto;

public class ProducerConfigDTO {
    private final int numMessages;
    private final long generationTimeoutInSeconds;
    private final int producersNum;

    public ProducerConfigDTO(int numMessages, long generationTimeout, int producersNum) {
        this.numMessages = numMessages;
        this.generationTimeoutInSeconds = generationTimeout;
        this.producersNum = producersNum;
    }

    public int numMessages() {
        return numMessages;
    }

    public long generationTimeoutInSeconds() {
        return generationTimeoutInSeconds;
    }

    public int producersNum() {
        return producersNum;
    }
}