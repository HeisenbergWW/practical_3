package com.iafanasiev.dto;

import com.iafanasiev.validator.eddr.ValidEddr;
import com.iafanasiev.validator.hasletter.HasLetter;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.PastOrPresent;
import org.hibernate.validator.constraints.Length;


import java.time.LocalDateTime;

public class QueueMessageDTO {
    @NotNull(message = "Name cannot be null")
    @Length(min = 7, message = "Length of name must be between 7 and 2147483647")
    @HasLetter(value = 'a', message = "{validation.customValidator.hasLetter.message}")
    private String name;

    @ValidEddr
    @NotNull
    String eddr;

    @Min(value = 10, message = "Count must be greater than or equal to 10")
    private int count;

    @PastOrPresent
    private LocalDateTime createdAt;

    /**
     * Default constructor needed for Jackson deserialization.
     * Jackson uses this constructor to create a new instance of the class
     * when deserializing JSON into a Java object.
     */
    @SuppressWarnings("unused")
    public QueueMessageDTO() {

    }

    public QueueMessageDTO(String name, String eddr, int count, LocalDateTime createdAt) {
        this.name = name;
        this.eddr = eddr;
        this.count = count;
        this.createdAt = createdAt;
    }

    public String getName() {
        return name;
    }

    @SuppressWarnings("unused")
    public String getEddr() {
        return eddr;
    }

    public int getCount() {
        return count;
    }

    @SuppressWarnings("unused")
    public LocalDateTime getCreatedAt() {
        return createdAt;
    }
}