package com.iafanasiev.loader;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Properties;

public class PropertiesLoader {
    private static final Logger logger = LoggerFactory.getLogger(PropertiesLoader.class);

    public static Properties loadProperties(String fileName) throws FileNotFoundException {
        logger.info("Loading properties");
        Properties properties = new Properties();

        logger.debug("Get the stream from the .properties through the class-loader");
        InputStream inputStream = PropertiesLoader.class.getClassLoader().getResourceAsStream(fileName);

        try {
            logger.debug("Load properties");
            properties.load(new InputStreamReader(inputStream, StandardCharsets.UTF_8));
        } catch (NullPointerException | IOException e) {
            throw new FileNotFoundException("Could not find properties file: " + fileName);
        }

        return properties;
    }

    PropertiesLoader() {
        throw new IllegalStateException("Utility class");
    }
}
