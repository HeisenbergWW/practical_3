package com.iafanasiev.validator.eddr;


import jakarta.validation.Constraint;
import jakarta.validation.Payload;

import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = EddrValidator.class)
@Target({ElementType.FIELD, ElementType.METHOD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
public @interface ValidEddr {
    String message() default "Invalid EDDR";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}