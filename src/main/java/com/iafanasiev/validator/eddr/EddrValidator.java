package com.iafanasiev.validator.eddr;

import com.iafanasiev.util.EddrObject;
import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;


import java.time.LocalDate;
import java.time.YearMonth;

public class EddrValidator implements ConstraintValidator<ValidEddr, String> {
    static String violationOfDateMsg;

    @Override
    public boolean isValid(String eddr, ConstraintValidatorContext context) {
        if (eddr == null) {

            return true;
        }

        if (!eddr.matches("\\d{8}-\\d{5}") && !eddr.matches("\\d{13}")) {
            context.disableDefaultConstraintViolation();
            context.buildConstraintViolationWithTemplate("Eddr must match the pattern 'YYYYMMDD-NNNNN' or 'YYYYMMDDNNNNN'")
                    .addConstraintViolation();
            return false;
        }

        EddrObject eddrObject = new EddrObject(eddr);


        if (!isValidDate(eddrObject.getDatePart())) {
            context.disableDefaultConstraintViolation();
            context.buildConstraintViolationWithTemplate(violationOfDateMsg)
                    .addConstraintViolation();
            return false;
        }

        int expectedControlNum = EddrObject.calculateControlNum(eddrObject.getDatePart() + eddrObject.getRecordNum());

        if (eddrObject.getControlNum() != expectedControlNum) {
            context.disableDefaultConstraintViolation();
            context.buildConstraintViolationWithTemplate("Invalid control number in EDDR")
                    .addConstraintViolation();
            return false;
        }

        return true;
    }

    static boolean isValidDate(String datePart) {
        int year = Integer.parseInt(datePart.substring(0, 4));
        int maxAge = 150;
        if (year < LocalDate.now().getYear() - maxAge || year > LocalDate.now().getYear()) {
            violationOfDateMsg = "Invalid year in EDDR";
            return false;
        }

        int month = Integer.parseInt(datePart.substring(4, 6));
        if (month < 1 || month > 12) {
            violationOfDateMsg = "Invalid month in EDDR";
            return false;
        }

        int day = Integer.parseInt(datePart.substring(6, 8));

        return isValidDayOfMonth(year, month, day);
    }

    static boolean isValidDayOfMonth(int year, int month, int day) {
        YearMonth yearMonth = YearMonth.of(year, month);

        violationOfDateMsg = "Invalid day in EDDR";
        return day >= 1 && day <= yearMonth.lengthOfMonth();
    }
}