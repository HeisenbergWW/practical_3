package com.iafanasiev.validator.hasletter;

import jakarta.validation.Constraint;
import jakarta.validation.Payload;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.*;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;


@Target({ METHOD, FIELD, ANNOTATION_TYPE })
@Retention(RUNTIME)
@Constraint(validatedBy = HasLetterValidator.class)
@Documented
public @interface HasLetter {

    String message() default "The string must contain the specified letter: '{value}'";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    char value();
}