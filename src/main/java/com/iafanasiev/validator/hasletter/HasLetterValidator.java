package com.iafanasiev.validator.hasletter;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;


public class HasLetterValidator implements ConstraintValidator<HasLetter, String> {

    char letter;

    @Override
    public void initialize(HasLetter constraintAnnotation) {
        this.letter = constraintAnnotation.value();
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        if (value == null) {
            return true;
        }
        return value.indexOf(letter) >= 0;
    }
}